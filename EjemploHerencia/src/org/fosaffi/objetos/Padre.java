/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.objetos;

/**
 *
 * @author jonathan
 */
public class Padre {

    String colorOjos;
    String colorPiel;
    String colorPelo;

    public String getColorojos() {
        return colorOjos;
    }

    public void setColorojos(String colorojos) {
        this.colorOjos = colorojos;
    }

    public String getColorPiel() {
        return colorPiel;
    }

    public void setColorPiel(String colorPiel) {
        this.colorPiel = colorPiel;
    }

    public String getColorPelo() {
        return colorPelo;
    }

    public void setColorPelo(String colorPelo) {
        this.colorPelo = colorPelo;
    }

    public Padre() {
    }

    public Padre(String ojos, String piel, String pelo) {
        this.colorOjos = ojos;
        this.colorPiel = piel;
        this.colorPelo = pelo;
    }

    public void caracteristicas() {
        System.out.println("mi color de ojos es: " + this.colorOjos);
        System.out.println("mi color de piel es: " + this.colorPiel);
        System.out.println("mi color de pelo es: " + this.colorPelo);
    }
}

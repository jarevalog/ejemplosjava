/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.objetos;

import org.fosaffi.objetos.Padre;

/**
 *
 * @author jonathan
 */
public class Hijo extends Padre {

    int numHermanos;

    public int getNumHermanos() {
        return numHermanos;
    }

    public void setNumHermanos(int numHermanos) {
        this.numHermanos = numHermanos;
    }

    public Hijo() {
    }

    public Hijo(String colorHojos, String colorPiel, String colorPelo) {
        super(colorHojos, colorPiel, colorPelo);
    }

}

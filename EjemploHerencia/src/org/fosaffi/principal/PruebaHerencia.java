/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.principal;

import org.fosaffi.objetos.Hijo;

/**
 *
 * @author jonathan
 */
public class PruebaHerencia {
    public static void main(String args[]) {
        Hijo hijo = new Hijo("verdes", "blanca", "rojo");
        hijo.setNumHermanos(1);
        hijo.caracteristicas();
    }
}

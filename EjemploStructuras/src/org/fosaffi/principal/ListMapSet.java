/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.principal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author jonathan
 */
public class ListMapSet {
        public static void main(String[] args) throws InterruptedException {

        System.err.println("Ejemplo de lista, se recorre con foreach e iterator");
        List<String> lista = new ArrayList();

        lista.add("uno");
        lista.add("dos");
        lista.add("tres");
        lista.add("cuatro");
        lista.add("cinco");
        Thread.sleep(500);
        System.out.println("\ttamanio de la lista: " + lista.size());
        int i = 0;
        System.out.println("\t>>>>>Lista con foreach");
        for (String s : lista) {
            System.out.println("\tlista en posicion [" + i + "]: " + s);
            i++;
        }
        System.out.println("\t>>>>>lista con iterator");
        Iterator<String> lstIterator = lista.iterator();
        while (lstIterator.hasNext()) {
            System.out.println("\tIterando lista con iterator, elemento: " + lstIterator.next());
        }
        Thread.sleep(2000);
        System.err.println("Ejemplo de lista enlazada, se recorre con foreach e iterator");
        LinkedList<String> listaEnlazada = new LinkedList<>();
        listaEnlazada.add("uno");
        listaEnlazada.add("dos");
        listaEnlazada.add("tres");
        listaEnlazada.add("cuatro");
        listaEnlazada.add("cinco");

        System.out.println("\ttamanio de la lista enlazada: " + listaEnlazada.size());
        i = 0;
        System.out.println("\t>>>>>Lista enlazada con foreach");
        for (String s : listaEnlazada) {
            System.out.println("\tlista enlazada en posicion [" + i + "]: " + s);
            i++;
        }
        System.out.println("\t>>>>>Lista enlazada con iterator");
        Iterator<String> lnkLstIterator = listaEnlazada.iterator();
        while (lnkLstIterator.hasNext()) {
            System.out.println("\tIterando lista enlazada con iterator, elemento: " + lnkLstIterator.next());
        }
        Thread.sleep(2000);
         System.err.println("Ejemplo de set (hash_set), se recorre con foreach e iterator");
        Set<String> hash_Set = new HashSet<String>(); 
        hash_Set.add("Geeks"); 
        hash_Set.add("For"); 
        hash_Set.add("Geeks"); 
        hash_Set.add("Example"); 
        hash_Set.add("Set"); 
         System.out.println("\ttamanio del hash set: " + hash_Set.size());
        i = 0;
        System.out.println("\t>>>>>hash_Set con foreach");
        for (String s : hash_Set) {
            System.out.println("\tlista enlazada en posicion [" + i + "]: " + s);
            i++;
        }
        System.out.println("\t>>>>>Hash_Set con iterator");
        Iterator<String> hSetIterator = hash_Set.iterator();
        while (hSetIterator.hasNext()) {
            System.out.println("\tIterando hash set con iterator, elemento: " + hSetIterator.next());
        }
        
        Thread.sleep(2000);
        System.err.println("Ejemplo de mapa enlazado, se recorre con foreach e iterator");
        LinkedHashMap<String, String> lhMap = new LinkedHashMap<String, String>();
        lhMap.put("b", "be");
        lhMap.put("z", "zeta");
        lhMap.put("s", "ese");
        lhMap.put("c", "ce");
        lhMap.put("a", "a");

        System.out.println("\ttamanio del mapa enlazado: " + lhMap.size());

        System.out.println("\t>>>>>LinkedHashMap con foreach");
        for (HashMap.Entry<String, String> entry : lhMap.entrySet()) {
            System.out.println("\tKey = " + entry.getKey()
                    + ", Value = " + entry.getValue());
        }

        System.out.println("\t>>>>>LinkedHashMap con iterator");
        Iterator<Map.Entry<String, String>> lhMapIter = lhMap.entrySet().iterator();
        while (lhMapIter.hasNext()) {
            Map.Entry<String, String> entry = lhMapIter.next();
            System.out.println("\tKey = " + entry.getKey()
                    + ", Value = " + entry.getValue());
        }
        
        Thread.sleep(2000);
        System.err.println("Ejemplo de mapa hash, se recorre con foreach e iterator");
        HashMap<String, String> hMap = new HashMap<String, String>();
        hMap.put("b", "be");
        hMap.put("z", "zeta");
        hMap.put("s", "ese");
        hMap.put("c", "ce");
        hMap.put("a", "a");

        System.out.println("\ttamanio del mapa hash: " + hMap.size());
        System.out.println("\t>>>>>HashMap con foreach");
        for (HashMap.Entry<String, String> entry : hMap.entrySet()) {
            System.out.println("\tKey = " + entry.getKey()
                    + ", Value = " + entry.getValue());
        }

        System.out.println("\t>>>>>HashMap con iterator");
        Iterator<Map.Entry<String, String>> hMapIter = hMap.entrySet().iterator();
        while (hMapIter.hasNext()) {
            Map.Entry<String, String> entry = hMapIter.next();
            System.out.println("\tKey = " + entry.getKey()
                    + ", Value = " + entry.getValue());
        }
        Thread.sleep(2000);
        System.err.println("Ejemplo de tree map, se recorre con foreach e iterator");

        TreeMap<String, String> tMap = new TreeMap<>();
        tMap.put("b", "be");
        tMap.put("z", "zeta");
        tMap.put("s", "ese");
        tMap.put("c", "ce");
        tMap.put("a", "a");
        System.out.println("\ttamanio del mapa tree: " + tMap.size());
        System.out.println("\t>>>>>TreeMap con foreach");
        for (Map.Entry<String, String> entry : tMap.entrySet()) {
            System.out.println("\tKey = " + entry.getKey()
                    + ", Value = " + entry.getValue());
        }

        System.out.println("\t>>>>>TreeMap con iterator");
        Iterator<Map.Entry<String, String>> tMapIter = tMap.entrySet().iterator();
        while (tMapIter.hasNext()) {
            Map.Entry<String, String> entry = tMapIter.next();
            System.out.println("\tKey = " + entry.getKey()
                    + ", Value = " + entry.getValue());
        }
    }
}

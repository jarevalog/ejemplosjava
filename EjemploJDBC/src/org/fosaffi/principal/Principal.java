/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.principal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.fosaffi.conexion.Conectar;

/**
 *
 * @author jonathan
 */
public class Principal {

    public static void main(String[] args) throws SQLException {
        //conectarse a la base de datos y consultar la tabla productos.
        Connection con = null;
        Conectar c = new Conectar();
        con = c.conectarBase();
        Statement s = null;
        ResultSet rs = null;
        try {
            s = con.createStatement();
            rs = s.executeQuery("select * from productos");
            System.out.println("id \tnombre \tstock \t categoria\t estado");
            while(rs.next()){
                System.out.print(rs.getInt("id_producto"));
                System.out.print("\t");
                System.out.print(rs.getString("nombre"));               
                System.out.print("\t");
                System.out.print(rs.getFloat("stock"));               
                System.out.print("\t\t");
                System.out.print(rs.getInt("id_categoria"));
                System.out.print("\t");
                System.out.print(rs.getInt("id_estado"));
                System.out.println("");                
            }
        } catch (Exception e) {
            System.err.println("Error. " + e.getMessage());
        } finally {
            con.close();
            s.close();
            rs.close();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jonathan
 */
public class Conectar {

    public Connection conectarBase() {
        String usuario = "sa";
        String password = "admin123";
        Connection cn = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            cn = DriverManager.getConnection("jdbc:sqlserver://DESKTOP-RA8CT34:1433;databaseName=capacitacion", usuario, password);
            System.out.println("Conectado.");
        } catch (SQLException ex) {
            System.out.println("Error." + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println("Error." + ex.getMessage());
        }
        return cn;
    }

    public Conectar() {
    }

}

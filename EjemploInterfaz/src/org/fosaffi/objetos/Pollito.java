/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.objetos;

import org.fosaffi.interfaz.Cantante;

/**
 *
 * @author jonathan
 */
public class Pollito implements Cantante {
    //atributos
    String tamanio;
    String colorPlumas;
    String edad;

    public String getTamanio() {
        return tamanio;
    }

    public void setTamanio(String tamanio) {
        this.tamanio = tamanio;
    }

    public String getColorPlumas() {
        return colorPlumas;
    }

    public void setColorPlumas(String colorPlumas) {
        this.colorPlumas = colorPlumas;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public Pollito() {
    }
    
    @Override
    public void cantar() {
        System.out.println("Poito cantando: PioPioPio PioPio Pio ...!!!");
    }
    //atributos
    
}

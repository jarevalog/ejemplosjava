/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.objetos;

import org.fosaffi.interfaz.Cantante;

/**
 *
 * @author jonathan
 */
public class Persona implements Cantante {
    //atributos
    String colorOjos;
    String colorPiel;
    String colorPelo;

    public String getColorOjos() {
        return colorOjos;
    }

    public void setColorOjos(String colorOjos) {
        this.colorOjos = colorOjos;
    }

    public String getColorPiel() {
        return colorPiel;
    }

    public void setColorPiel(String colorPiel) {
        this.colorPiel = colorPiel;
    }

    public String getColorPelo() {
        return colorPelo;
    }

    public void setColorPelo(String colorPelo) {
        this.colorPelo = colorPelo;
    }

    public Persona() {
    }

    @Override
    public void cantar() {
        System.out.println("Persona cantando: Lalala Lala La ... !!!");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.principal;

import org.fosaffi.objetos.Pollito;
import org.fosaffi.objetos.Persona;

/**
 *
 * @author jonathan
 */
public class PruebaInterfaz {
    public static void main(String args[]){
        Persona persona = new Persona();
        persona.cantar();
        Pollito poito = new Pollito();
        poito.cantar();
    }
}

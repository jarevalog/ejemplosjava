/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.fosaffi.conexion.Conectar;
import org.fosaffi.pojo.Productos;

/**
 *
 * @author jonathan
 */
public class PorductosDao {

    public List<Productos> getProductos() throws SQLException {
        List<Productos> lista = new ArrayList<>();

        //conectarse a la base de datos y consultar la tabla productos.
        Connection con = null;
        Conectar c = new Conectar();
        con = c.conectarBase();
        Statement s = null;
        ResultSet rs = null;
        try {
            s = con.createStatement();
            rs = s.executeQuery("select * from productos");
            while (rs.next()) {
                Productos producto = new Productos();
                producto.setIdProducto(rs.getInt("id_producto"));
                producto.setNombre(rs.getString("nombre"));
                producto.setStock(rs.getFloat("stock"));
                producto.setCategoria(rs.getInt("id_categoria"));
                producto.setEstado(rs.getInt("id_estado"));
                lista.add(producto);
            }
        } catch (Exception e) {
            System.err.println("Error. " + e.getMessage());
        } finally {
            con.close();
            s.close();
            rs.close();
        }
        return lista;
    }
}

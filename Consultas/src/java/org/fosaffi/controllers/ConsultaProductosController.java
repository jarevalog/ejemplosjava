/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.controllers;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.fosaffi.dao.PorductosDao;
import org.fosaffi.pojo.Productos;

/**
 *
 * @author jonathan
 */
@Named("consultas")
@SessionScoped
public class ConsultaProductosController implements Serializable {

    List<Productos> lstProductos;

    public List<Productos> getLstProductos() {
        return lstProductos;
    }

    public void setLstProductos(List<Productos> lstProductos) {
        this.lstProductos = lstProductos;
    }

    public ConsultaProductosController() {
    }

    public void procesar() {
        try {
            PorductosDao pDao = new PorductosDao();
            lstProductos = pDao.getProductos();
        } catch (Exception e) {
            System.err.println("Error " + e.getMessage());
        }
    }

}

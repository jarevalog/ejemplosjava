/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.pojo;

import java.util.Date;
import org.primefaces.model.DefaultScheduleEvent;

/**
 *
 * @author jonathan
 */
public class TablaEventosPojo {

    Usuario empleado;
    int dias;
    int estadoCalendario;
    int tipo;
    DefaultScheduleEvent evento;
    int acumulado;

    public TablaEventosPojo() {
    }

    public int getAcumulado() {
        return acumulado;
    }

    public void setAcumulado(int acumulado) {
        this.acumulado = acumulado;
    }

    public DefaultScheduleEvent getEvento() {
        return evento;
    }

    public void setEvento(DefaultScheduleEvent evento) {
        this.evento = evento;
    }

    public Usuario getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Usuario empleado) {
        this.empleado = empleado;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }

    public int getEstadoCalendario() {
        return estadoCalendario;
    }

    public void setEstadoCalendario(int estadoCalendario) {
        this.estadoCalendario = estadoCalendario;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

}

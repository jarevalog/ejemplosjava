/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.pojo;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.fosaffi.entity.CalendarioVacacion;
import org.fosaffi.entity.Programa;
import org.fosaffi.entity.VacacionEmpleado;

/**
 *
 * @author jonathan
 */
@Named("usuario")
@SessionScoped
public class Usuario implements Serializable {

    String rol;
    String nombre;
    int codigoEmpresa;
    int codigoEmpleado;
    int codigoUnidad;
    int diasCorresponden;
    VacacionEmpleado periodoVacacionEmpleado;
    List<Programa> programas;
    List<CalendarioVacacion> vacaciones;

    public Usuario() {
    }

    public int getDiasCorresponden() {
        return diasCorresponden;
    }

    public void setDiasCorresponden(int diasCorresponden) {
        this.diasCorresponden = diasCorresponden;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(int codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public int getCodigoEmpleado() {
        return codigoEmpleado;
    }

    public void setCodigoEmpleado(int codigoEmpleado) {
        this.codigoEmpleado = codigoEmpleado;
    }

    public int getCodigoUnidad() {
        return codigoUnidad;
    }

    public void setCodigoUnidad(int codigoUnidad) {
        this.codigoUnidad = codigoUnidad;
    }

    public List<Programa> getProgramas() {
        return programas;
    }

    public void setProgramas(List<Programa> programas) {
        this.programas = programas;
    }

    public VacacionEmpleado getPeriodoVacacionEmpleado() {
        return periodoVacacionEmpleado;
    }

    public void setPeriodoVacacionEmpleado(VacacionEmpleado periodoVacacionEmpleado) {
        this.periodoVacacionEmpleado = periodoVacacionEmpleado;
    }

    public List<CalendarioVacacion> getVacaciones() {
        return vacaciones;
    }

    public void setVacaciones(List<CalendarioVacacion> vacaciones) {
        this.vacaciones = vacaciones;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.fosaffi.entity.CalendarioVacacion;
import org.fosaffi.entity.PeriodoVacacion;
import org.fosaffi.entity.Programa;
import org.fosaffi.entity.ProgramaPK;
import org.fosaffi.entity.VacacionEmpleado;
import org.fosaffi.entity.VacacionEmpleadoPK;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.fosaffi.pojo.TablaEventosPojo;
import org.fosaffi.pojo.Usuario;
import org.fosaffi.service.CalendarioVacacionFacadeLocal;
import org.fosaffi.service.EstadoVacacionFacadeLocal;
import org.fosaffi.service.PeriodoVacacionFacadeLocal;
import org.fosaffi.service.ProgramaFacadeLocal;
import org.fosaffi.service.TipoProgramaFacadeLocal;
import org.fosaffi.service.VacacionEmpleadoFacadeLocal;

/**
 *
 * @author jonathan
 */
@Named("calendario")
@SessionScoped
public class ScheduleController implements Serializable {

    @Inject
    private Usuario usuario;

    @EJB
    ProgramaFacadeLocal programaEJB;
    @EJB
    VacacionEmpleadoFacadeLocal vacacionEmpleadoEJB;
    @EJB
    CalendarioVacacionFacadeLocal calendarioVacacionEJB;
    @EJB
    EstadoVacacionFacadeLocal estadoEJB;
    @EJB
    TipoProgramaFacadeLocal tipoEJB;
    @EJB
    PeriodoVacacionFacadeLocal periodoEJB;

    Date initDate;

    private Date fechaInicio;

    private Date fechaFin;

    private ScheduleModel eventModel;

    private ScheduleEvent event = new DefaultScheduleEvent();

    TablaEventosPojo selectedEvent;

    Date today;

    List<TablaEventosPojo> tablaEventosList;

    List<TablaEventosPojo> tablaEventosReprogramadosList;

    List<TablaEventosPojo> eventosReprogramarList;

    List<TablaEventosPojo> otrosEventosList;

    List<String> lstEmpleados;

    List<String> lstSelectedEmpleados;

    TablaEventosPojo selected;

    HashMap<String, List<TablaEventosPojo>> filtro;

    HashMap mapTipos;

    HashMap mapEstados;

    int totalDiasAcumulados;

    int totalDiasAcumuladosReprogramacion;

    public int getTotalDiasAcumuladosReprogramacion() {
        return totalDiasAcumuladosReprogramacion;
    }

    public void setTotalDiasAcumuladosReprogramacion(int totalDiasAcumuladosReprogramacion) {
        this.totalDiasAcumuladosReprogramacion = totalDiasAcumuladosReprogramacion;
    }

    public List<TablaEventosPojo> getEventosReprogramarList() {
        return eventosReprogramarList;
    }

    public void setEventosReprogramarList(List<TablaEventosPojo> eventosReprogramarList) {
        this.eventosReprogramarList = eventosReprogramarList;
    }

    public int getTotalDiasAcumulados() {
        return totalDiasAcumulados;
    }

    public void setTotalDiasAcumulados(int totalDiasAcumulados) {
        this.totalDiasAcumulados = totalDiasAcumulados;
    }

    public HashMap getMapTipos() {
        return mapTipos;
    }

    public void setMapTipos(HashMap mapTipos) {
        this.mapTipos = mapTipos;
    }

    public HashMap getMapEstados() {
        return mapEstados;
    }

    public void setMapEstados(HashMap mapEstados) {
        this.mapEstados = mapEstados;
    }

    public List<String> getLstSelectedEmpleados() {
        return lstSelectedEmpleados;
    }

    public void setLstSelectedEmpleados(List<String> lstSelectedEmpleados) {
        this.lstSelectedEmpleados = lstSelectedEmpleados;
    }

    public List<String> getLstEmpleados() {
        return lstEmpleados;
    }

    public void setLstEmpleados(List<String> lstEmpleados) {
        this.lstEmpleados = lstEmpleados;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public TablaEventosPojo getSelected() {
        return selected;
    }

    public void setSelected(TablaEventosPojo selected) {
        this.selected = selected;
    }

    public List<TablaEventosPojo> getTablaEventosList() {
        return tablaEventosList;
    }

    public void setTablaEventosList(List<TablaEventosPojo> tablaEventosList) {
        this.tablaEventosList = tablaEventosList;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public ScheduleModel getEventModel() {
        return eventModel;
    }

    public HashMap<String, List<TablaEventosPojo>> getFiltro() {
        return filtro;
    }

    public void setFiltro(HashMap<String, List<TablaEventosPojo>> filtro) {
        this.filtro = filtro;
    }

    public void setEventModel(ScheduleModel eventModel) {
        this.eventModel = eventModel;
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    public TablaEventosPojo getSelectedEvent() {
        return selectedEvent;
    }

    public void setSelectedEvent(TablaEventosPojo selectedEvent) {
        this.selectedEvent = selectedEvent;
    }

    public Date getToday() {
        return today;
    }

    public void setToday(Date today) {
        this.today = today;
    }

    public List<TablaEventosPojo> getTablaEventosReprogramadosList() {
        return tablaEventosReprogramadosList;
    }

    public void setTablaEventosReprogramadosList(List<TablaEventosPojo> tablaEventosReprogramadosList) {
        this.tablaEventosReprogramadosList = tablaEventosReprogramadosList;
    }

    @PostConstruct//for test, quitar cuando este en produccion
    public void Inicialize() {
        usuario = new Usuario();
        usuario.setCodigoEmpleado(1);
        usuario.setCodigoEmpresa(1);
        usuario.setCodigoUnidad(7);
        usuario.setNombre("PRUEBA");
        usuario.setRol("EMPLEADO");
        usuario.setDiasCorresponden(12);
        //llenar tipos de programas
        mapTipos = new HashMap();
        tipoEJB.findAll().forEach(item -> mapTipos.put(item.getId(), item.getTipo()));
        //llenar estados de vacaciones
        mapEstados = new HashMap();
        estadoEJB.findAll().forEach(item -> mapEstados.put(item.getId(), item.getEstado()));
    }

    public void init() {
        if (isNotPostback()) {
            tablaEventosReprogramadosList = new ArrayList<>();
            eventosReprogramarList = new ArrayList<>();
            totalDiasAcumulados = 0;
            //traer numero maximo de dias que se pueden gozar
            selected = new TablaEventosPojo();
            //tablaEventosList = new ArrayList<>();
            today = new Date();
            eventModel = new DefaultScheduleModel();
            lstSelectedEmpleados = new ArrayList<>();
            lstEmpleados = new ArrayList<>();
            if (usuario.getRol().equals("JEFE") || usuario.getRol().equals("RRHH")) {
                //traer lista de empleados de su unidad con su respectiva lista de eventos
                initDataEmpleado();
                //filtros
                filtro = new HashMap();
                for (TablaEventosPojo tep : otrosEventosList) {
                    int dias = Days.daysBetween(new LocalDate(tep.getEvento().getStartDate()), new LocalDate(tep.getEvento().getEndDate())).getDays();
                    dias++;
                    tep.setDias(dias);
                    Date fechaFinMostrar = new Date();
                    fechaFinMostrar = getFechaFinMostrarSchedule(tep.getEvento().getEndDate());
                    if (tep.getEstadoCalendario() != 2) {
                        eventModel.addEvent(new DefaultScheduleEvent(tep.getEvento().getTitle(), tep.getEvento().getStartDate(), fechaFinMostrar));
                    }
                    //para filtrar
                    List<TablaEventosPojo> listaEventos = new ArrayList<>();
                    listaEventos = filtro.get(tep.getEvento().getTitle());
                    if (listaEventos == null) {
                        listaEventos = new ArrayList<>();
                        listaEventos.add(tep);
                    } else {
                        listaEventos.add(tep);
                    }
                    filtro.put(tep.getEvento().getTitle(), listaEventos);
                }
                filtro.forEach((k, v) -> lstEmpleados.add(k));
                tablaEventosList = otrosEventosList;
            } else {
                initDataEmpleado();
                try {
                    initDate = new Date();
                    //eventList.add(new DefaultScheduleEvent("Champions League Match 2", new SimpleDateFormat("dd/MM/yyyy").parse("10/12/2019"),
                    // new SimpleDateFormat("dd/MM/yyyy").parse("20/12/2019"), "Team A vs. Team B"));
                } catch (Exception ex) {
                    Logger.getLogger(ScheduleController.class.getName()).log(Level.SEVERE, null, ex);
                }
                /*DefaultScheduleEvent evento = new DefaultScheduleEvent("Champions League Match 3", new Date(), new Date(), "Team A vs. Team B");
                evento.setStyleClass("dateSelected");*/
                //eventList.add(evento);

                /*for (DefaultScheduleEvent event : eventList) {
            eventModel.addEvent(event);
        }*/
                for (TablaEventosPojo tep : tablaEventosList) {
                    int dias = Days.daysBetween(new LocalDate(tep.getEvento().getStartDate()), new LocalDate(tep.getEvento().getEndDate())).getDays();
                    dias++;
                    tep.setDias(dias);
                    if (tep.getEstadoCalendario() != 2) {
                        totalDiasAcumulados += dias;
                    }
                    Date fechaFinMostrar = new Date();
                    fechaFinMostrar = getFechaFinMostrarSchedule(tep.getEvento().getEndDate());
                    if (tep.getEstadoCalendario() != 2) {
                        eventModel.addEvent(new DefaultScheduleEvent(tep.getEvento().getTitle(), tep.getEvento().getStartDate(), fechaFinMostrar));
                    }
                }
            }
        }
    }

    public void initModal() {
        totalDiasAcumuladosReprogramacion = 0;
        for (TablaEventosPojo tep : eventosReprogramarList) {
            totalDiasAcumuladosReprogramacion += tep.getDias();
            Iterator<TablaEventosPojo> it = otrosEventosList.iterator();
            while (it.hasNext()) {
                TablaEventosPojo tep2 = it.next();
                if (tep.getEvento().getId() == tep2.getEvento().getId()) {
                    it.remove();
                }
            }
        }

    }

    public void onDateSelect(SelectEvent selectEvent) {
        fechaFin = (Date) fechaInicio.clone();
    }

    public void onRowSelect(SelectEvent evento) {
        TablaEventosPojo eSelected = new TablaEventosPojo();
        eSelected = (TablaEventosPojo) evento.getObject();
        System.err.println("");
        initDate = eSelected.getEvento().getStartDate();
        // FacesMessage msg = new FacesMessage("Car Selected", event.getObject().getId());
        // FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void agregar() {
        //Long dias = ((selected.getEvento().getStartDate().getTime() - selected.getEvento().getEndDate().getTime()) / 86400000);
        //validar fechas con eventos de otros usuarios
        if (esFechaValida(fechaInicio, fechaFin, otrosEventosList)) {
            //validar fechas con eventos del mismo usuario
            if (esFechaValida(fechaInicio, fechaFin, tablaEventosList)) {
                initDate = fechaInicio;
                int days = Days.daysBetween(new LocalDate(fechaInicio), new LocalDate(fechaFin)).getDays();
                days++;
                totalDiasAcumulados += days;
                if (totalDiasAcumulados > usuario.getDiasCorresponden()) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Usuario no puede reservas mas dias de los que le corresponden !!!."));
                    totalDiasAcumulados -= days;
                    return;
                }
                selected.setDias(days);
                selected.setEmpleado(usuario);
                selected.setEstadoCalendario(0);
                DefaultScheduleEvent evento
                        = new DefaultScheduleEvent("Vacacion", fechaInicio,
                                fechaFin, "Team A vs. Team B");
                selected.setEvento(evento);
                tablaEventosList.add(selected);
                //para mostrar
                Date fechaFinMostrar = new Date();
                fechaFinMostrar = getFechaFinMostrarSchedule(fechaFin);
                DefaultScheduleEvent eventoMostrar = new DefaultScheduleEvent("Vacacion", fechaInicio,
                        fechaFinMostrar, "Team A vs. Team B");
                eventModel.addEvent(eventoMostrar);

                selected = new TablaEventosPojo();
                fechaInicio = null;
                fechaFin = null;
                //revision de acumulados   
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "No es posible agregar esta fecha, fechas ocupadas !!!."));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "No es posible agregar esta fecha, fechas ocupadas !!!."));

        }
    }

    public Boolean isNotPostback() {
        return !FacesContext.getCurrentInstance().isPostback();
    }

    //valida que las fechas de las vacaciones no choquen
    // public static boolean esFechaValida(Date pFechaIni, Date pFechaFin) {
    public static boolean esFechaValida(Date pFechaIni, Date pFechaFin,
            List<TablaEventosPojo> list) {
        //uncoment tablaEventosList = list; for test*/
        //tablaEventosList = list;
        boolean esValida = true;
        /*between in java
        Date min, max;   // assume these are set to something
        Date d;          // the date in question

        return d.after(min) && d.before(max);
         */
        //15 casos negativos, 4 casos positivo
        /*
        referencia: https://drive.google.com/file/d/1YVtliqMtmN1TxPo9YeWvwRuQ7Wv2zB4v/view?usp=sharing
         */
        for (TablaEventosPojo tep : list) {
            //caso de evaluacion
            Date fechaIniReferencia;
            Date fechaFinReferencia;
            fechaIniReferencia = tep.getEvento().getStartDate();
            fechaFinReferencia = tep.getEvento().getEndDate();
            // caso 0, el rango de fechas hace referencia a 1 solo dia.

            //------------------------------ CASOS 1 AL 4 ---------------------------------------
            //revisar si las fechas en las que se quieren ir en vacacion no chocan con rangos de fechas ya puestas
            boolean fechaIniInBeetwen = fechaIniReferencia.compareTo(pFechaIni) * pFechaIni.compareTo(fechaFinReferencia) > 0;//fecha inicio entre min - max
            boolean fechaFinInBeetwen = fechaIniReferencia.compareTo(pFechaFin) * pFechaFin.compareTo(fechaFinReferencia) > 0;//fecha fin entre min - max
            //si fechaIniInBeetwen y fechaFinInBeetwen son false ambos, podemos seguir, caso contrario, las fechas de vacacion chocan
            if (!fechaIniInBeetwen && !fechaFinInBeetwen) {//casos 1,2 y 3
                //validar que las fechas en las que se quieren ir de vacaciones no cubren rangos ya seleccionados
                boolean fechaIniRefInBeetwen = pFechaIni.compareTo(fechaIniReferencia) * fechaIniReferencia.compareTo(pFechaFin) > 0;
                boolean fechaFinRefInBeetwen = pFechaIni.compareTo(fechaFinReferencia) * fechaFinReferencia.compareTo(pFechaFin) > 0;
                if (fechaIniRefInBeetwen && fechaFinRefInBeetwen) {//caso 4
                    esValida = false;
                    break;
                }
                //nota=si no se encuentra en ninguno de los casos evaluados sera fecha valida
            } else {
                esValida = false;
                break;
            }
            // ---------------------------FIN CASOS 1 AL 4 ---------------------------------------
            // -------------CASOS 5 AL 9,12,13,14 y especiales, EVALUAN FRONTERAS DE FECHA---------------------------------------
            if (esValida) {//si es true seguir evaluando, caso contrario ya llego al fin
                //caso 5 ini en between frontera fin coincide
                boolean fechaFinEqFrontera = fechaFinReferencia.compareTo(pFechaFin) == 0;
                if (fechaIniInBeetwen && fechaFinEqFrontera) {
                    esValida = false;
                    break;
                }
                //caso 6 fin en between frontera inicio coincide
                boolean fechaIniEqFrontera = fechaIniReferencia.compareTo(pFechaIni) == 0;
                if (fechaFinInBeetwen && fechaIniEqFrontera) {
                    esValida = false;
                    break;
                }
                //caso 7 fonteras ini y fin coinciden
                if (fechaIniEqFrontera && fechaFinEqFrontera) {
                    esValida = false;
                    break;
                }
                //caso 8 fecha ini ok fecha fin coincide con ini frontera
                boolean fechaFinEqFronteraIni = fechaIniReferencia.compareTo(pFechaFin) == 0;
                if (!fechaIniInBeetwen && fechaFinEqFronteraIni) {
                    esValida = false;
                    break;
                }
                //caso 9 fecha fin ok fecha ini coincide con fin frontera
                boolean fechaIniEqFronteraFin = fechaFinReferencia.compareTo(pFechaIni) == 0;
                if (!fechaFinInBeetwen && fechaIniEqFronteraFin) {
                    esValida = false;
                    break;
                }

            }
            // -------------FIN CASOS 5 AL 9---------------------------------------
        }

        return esValida;
    }
    //validar numero maximo de dias que se puede ir de vacaciones

    //Convert Date to Calendar
    private Calendar dateToCalendar(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;

    }

    //Convert Calendar to Date
    private Date calendarToDate(Calendar calendar) {
        return calendar.getTime();
    }

    public void initDataEmpleado() {
        //traer datos del empleados en el periodo actual
        int year = Calendar.getInstance().get(Calendar.YEAR);
        usuario.setPeriodoVacacionEmpleado(vacacionEmpleadoEJB.findByPeriodo(year));
        //traer toda la data relacionada con ese empleado.
        usuario.setProgramas(programaEJB.findProgramasByEmpleadoPeriodo(usuario.getCodigoEmpresa(), usuario.getCodigoEmpleado(), year));
        //del programa sale el calendario
        usuario.setVacaciones(calendarioVacacionEJB.findVacacionesByEmpleadoProgram(usuario.getProgramas()));
        tablaEventosList = new ArrayList<>();
        //set tabla de eventos
        int acumulado = 0;
        for (CalendarioVacacion cv : usuario.getVacaciones()) {
            TablaEventosPojo tep = new TablaEventosPojo();
            tep.setTipo(cv.getPrograma().getProgramaPK().getTipo());
            tep.setEstadoCalendario(cv.getEstado());
            DefaultScheduleEvent dev = new DefaultScheduleEvent(usuario.getNombre(), cv.getFechaDesde(), cv.getFechaHasta());
            dev.setId(cv.getId().toString());
            tep.setEvento(dev);
            int days = Days.daysBetween(new LocalDate(fechaInicio), new LocalDate(fechaFin)).getDays();
            days++;
            if (cv.getEstado() != 2) {
                acumulado += days;
            }
            tep.setDias(days);
            tablaEventosList.add(tep);
        }
        //esta lista guardara todas las fechas del departamento, para que no existan choques de fechas de vacacion
        otrosEventosList = new ArrayList<>();

        otrosEventosList = calendarioVacacionEJB.findVacacionesByUnidad(usuario);

        System.out.println("org.fosaffi.controller.ScheduleController.initDataEmpleado()");
    }

    public void buscarEmpleados() {
        eventModel = new DefaultScheduleModel();
        tablaEventosList = new ArrayList<>();
        otrosEventosList = new ArrayList<>();
        for (String empleado : lstSelectedEmpleados) {
            tablaEventosList.addAll(filtro.get(empleado));
        }
        otrosEventosList = tablaEventosList;
        for (TablaEventosPojo tep : otrosEventosList) {
            int dias = Days.daysBetween(new LocalDate(tep.getEvento().getStartDate()), new LocalDate(tep.getEvento().getEndDate())).getDays();
            dias++;
            tep.setDias(dias);
            Date fechaFinMostrar = new Date();
            fechaFinMostrar = getFechaFinMostrarSchedule(tep.getEvento().getEndDate());
            eventModel.addEvent(new DefaultScheduleEvent(tep.getEvento().getTitle(), tep.getEvento().getStartDate(), fechaFinMostrar));
        }
    }

    public Date getFechaFinMostrarSchedule(Date fFin) {
        Date fechaFinMostrar = (Date) fFin.clone();
        Calendar c
                = dateToCalendar(fechaFinMostrar);
        c.add(Calendar.DAY_OF_YEAR, 1);
        fechaFinMostrar = calendarToDate(c);
        return fechaFinMostrar;
    }
    int tipo;

    public int determinaTipoPrograma(ProgramaPK pPk) {
        tipo = 0;
        try {
            List<Programa> programas = programaEJB.findProgramasByEmpleadoPeriodo(pPk.getCodEmp(), pPk.getCodEmple(), pPk.getCodPeriodo());
            programas.forEach(item -> {
                if (item.getTipoPrograma().getId() > tipo) {
                    tipo = item.getTipoPrograma().getId();
                }
            });
        } catch (Exception e) {
        }
        return tipo;
    }

    public void guardar() {
        //validar que el acumulado es igual a los dias posibles de reserva.
        if (totalDiasAcumulados == usuario.getDiasCorresponden()) {//proceder con las tareas de guardado
            //llenar las tablas vacacion empleado vacacion programa y vacacion calendario
            try {
                //Periodo
                PeriodoVacacion periodoVacacion = new PeriodoVacacion();
                periodoVacacion = periodoEJB.find(Calendar.getInstance().get(Calendar.YEAR));

                //vacaciones empleados
                VacacionEmpleadoPK vePk = new VacacionEmpleadoPK();
                vePk.setCodEmp(usuario.getCodigoEmpresa());
                vePk.setCodEmple(usuario.getCodigoEmpleado());
                vePk.setCodPeriodo(periodoVacacion.getCodPeriodo());
                VacacionEmpleado ve = new VacacionEmpleado();
                ve.setVacacionEmpleadoPK(vePk);
                ve.setPeriodoVacacion(periodoVacacion);
                ve.setDiasCorresponden(usuario.getDiasCorresponden());
                ve.setDiasVacacion(periodoVacacion.getDiasColectivos());//agrega
                ve.setFechaControl(new Date());
                ve.setProgramaList(new ArrayList<>());
                vacacionEmpleadoEJB.create(ve);

                //programa 
                ProgramaPK pPk = new ProgramaPK();
                pPk.setCodEmp(usuario.getCodigoEmpresa());
                pPk.setCodEmple(usuario.getCodigoEmpleado());
                pPk.setCodPeriodo(periodoVacacion.getCodPeriodo());
                pPk.setTipo(determinaTipoPrograma(pPk));
                Programa p = new Programa();
                p.setProgramaPK(pPk);
                p.setTipoPrograma(tipoEJB.find(determinaTipoPrograma(pPk)));
                p.setDiasPendientes(0);
                p.setFecha(new Date());
                p.setFechaControl(new Date());
                p.setCalendarioVacacionList(new ArrayList<>());
                //p.setVacacionEmpleado(new VacacionEmpleado());
                List<Programa> programas = new ArrayList<>();
                programas = programaEJB.findProgramasByEmpleadoPeriodo(pPk.getCodEmp(), pPk.getCodEmp(), periodoVacacion.getCodPeriodo());
                programas.add(p);
                ve.setProgramaList(programas);
                vacacionEmpleadoEJB.edit(ve);
                //programaEJB.create(p);
                //calendario vacaciones
                //lista actual
                List<CalendarioVacacion> listaCalendarioIngresar = new ArrayList<>();
                for (TablaEventosPojo tep : tablaEventosList) {
                    CalendarioVacacion cv = new CalendarioVacacion();
                    //cv.setId(1);
                    cv.setPrograma(p);
                    cv.setUsuarioCreacion(usuario.getNombre());
                    cv.setFechaCreacion(new Date());
                    cv.setEstado(1);
                    cv.setFechaDesde(tep.getEvento().getStartDate());
                    cv.setFechaHasta(tep.getEvento().getEndDate());
                    listaCalendarioIngresar.add(cv);
                }
//                List<CalendarioVacacion> listaCalendarioRegistrado = calendarioVacacionEJB.findVacacionesByEmpleadoProgram(programas);
//                listaCalendarioRegistrado.addAll(listaCalendarioIngresar);
                p = programaEJB.find(p.getProgramaPK());
                p.setCalendarioVacacionList(listaCalendarioIngresar);
                programaEJB.edit(p);

                System.out.println("Hecho !!!");
            } catch (Exception e) {
                System.out.println("Error al guardar ");
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Dias correspondintes de vacacion (" + usuario.getDiasCorresponden() + ") deben coincidir con dias programados/reservados (" + totalDiasAcumulados + ") !!!."));
        }
    }

    public void addReprogramar(TablaEventosPojo tep) {
        if (eventosReprogramarList == null) {
            eventosReprogramarList = new ArrayList<>();
        }
        //revisar si el objeto ya habia sido seleccionado.
        boolean encontrado = false;
        Iterator<TablaEventosPojo> it = eventosReprogramarList.iterator();
        while (it.hasNext()) {
            TablaEventosPojo aux = it.next();
            if (aux.getEvento().getId().equals(tep.getEvento().getId())) {
                it.remove();
                return;
            }
        }
        if (!encontrado) {
            eventosReprogramarList.add(tep);
        }
    }

    public void agregarReprogramacion() {
        //Long dias = ((selected.getEvento().getStartDate().getTime() - selected.getEvento().getEndDate().getTime()) / 86400000);
        //validar fechas con eventos de otros usuarios
        if (esFechaValida(fechaInicio, fechaFin, otrosEventosList)) {
            //validar fechas con eventos del mismo usuario
            if (esFechaValida(fechaInicio, fechaFin, tablaEventosReprogramadosList)) {
                initDate = fechaInicio;
                int days = Days.daysBetween(new LocalDate(fechaInicio), new LocalDate(fechaFin)).getDays();
                days++;
                totalDiasAcumuladosReprogramacion -= days;
                selected.setDias(days);
                selected.setEmpleado(usuario);
                selected.setEstadoCalendario(0);
                DefaultScheduleEvent evento
                        = new DefaultScheduleEvent("Vacacion", fechaInicio,
                                fechaFin, "Team A vs. Team B");
                selected.setEvento(evento);
                tablaEventosReprogramadosList.add(selected);
                //para mostrar
                Date fechaFinMostrar = new Date();
                fechaFinMostrar = getFechaFinMostrarSchedule(fechaFin);
                DefaultScheduleEvent eventoMostrar = new DefaultScheduleEvent("Vacacion", fechaInicio,
                        fechaFinMostrar, "Team A vs. Team B");
                //eventModel.addEvent(eventoMostrar);

                selected = new TablaEventosPojo();
                fechaInicio = null;
                fechaFin = null;
                //revision de acumulados   
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "No es posible agregar esta fecha, fechas ocupadas !!!."));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "No es posible agregar esta fecha, fechas ocupadas !!!."));
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public String guardaReprogramacion() {
        //validar que el acumulado es igual a los dias posibles de reserva.
        if (totalDiasAcumuladosReprogramacion == 0) {//proceder con las tareas de guardado
            //llenar las tablas vacacion empleado vacacion programa y vacacion calendario
            try {
                //Periodo
                PeriodoVacacion periodoVacacion = new PeriodoVacacion();
                periodoVacacion = periodoEJB.find(Calendar.getInstance().get(Calendar.YEAR));

                //vacaciones empleados
                VacacionEmpleadoPK vePk = new VacacionEmpleadoPK(usuario.getCodigoEmpresa(), usuario.getCodigoEmpleado(), periodoVacacion.getCodPeriodo());
                VacacionEmpleado ve = vacacionEmpleadoEJB.find(vePk);//programa 
                ProgramaPK pPk = new ProgramaPK();
                pPk.setCodEmp(usuario.getCodigoEmpresa());
                pPk.setCodEmple(usuario.getCodigoEmpleado());
                pPk.setCodPeriodo(periodoVacacion.getCodPeriodo());
                //determinar el tipo programa, maximo puede ser 3
                int tipoPrograma = determinaTipoPrograma(pPk) + 1;
                if (tipoPrograma > 3) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "No es posible realizar reprogramaciones, reprogramaciones posibles agotadas !!!."));
                    return "";
                }
                //actualiza la lista a reporgramar
                /*for (TablaEventosPojo tep : eventosReprogramarList) {
                    CalendarioVacacion cv = new CalendarioVacacion();
                    cv = calendarioVacacionEJB.find(Integer.valueOf(tep.getEvento().getId()));
                    cv.setEstado(3);//3 es reprogramado
                    int programaOriginal = tipoPrograma - 1;
                    ProgramaPK pPkOriginal = new ProgramaPK(pPk.getCodEmp(),pPk.getCodEmple(),pPk.getCodPeriodo(),programaOriginal);                    
                    Programa p = programaEJB.find(pPkOriginal);
                    cv.setPrograma(p);
                    calendarioVacacionEJB.edit(cv);
                }*/
                pPk.setTipo(tipoPrograma);
                Programa p = new Programa();
                p.setProgramaPK(pPk);
                p.setTipoPrograma(tipoEJB.find(tipoPrograma));
                p.setDiasPendientes(0);
                p.setFecha(new Date());
                p.setFechaControl(new Date());
                p.setCalendarioVacacionList(new ArrayList<>());
                //p.setVacacionEmpleado(new VacacionEmpleado());
                List<Programa> programas = new ArrayList<>();
                programas = programaEJB.findProgramasByEmpleadoPeriodo(pPk.getCodEmp(), pPk.getCodEmp(), periodoVacacion.getCodPeriodo());
                programas.add(p);
                ve.setProgramaList(programas);
                vacacionEmpleadoEJB.edit(ve);
                //programaEJB.create(p);
                //calendario vacaciones
                //lista actual
                List<CalendarioVacacion> listaCalendarioIngresar = new ArrayList<>();
                for (TablaEventosPojo tep : tablaEventosReprogramadosList) {
                    CalendarioVacacion cv = new CalendarioVacacion();
                    //cv.setId(1);
                    cv.setPrograma(p);
                    cv.setUsuarioCreacion(usuario.getNombre());
                    cv.setFechaCreacion(new Date());
                    cv.setEstado(1);
                    cv.setFechaDesde(tep.getEvento().getStartDate());
                    cv.setFechaHasta(tep.getEvento().getEndDate());
                    listaCalendarioIngresar.add(cv);
                }
//                List<CalendarioVacacion> listaCalendarioRegistrado = calendarioVacacionEJB.findVacacionesByEmpleadoProgram(programas);
//                listaCalendarioRegistrado.addAll(listaCalendarioIngresar);
                p = programaEJB.find(p.getProgramaPK());
                p.setCalendarioVacacionList(listaCalendarioIngresar);
                programaEJB.edit(p);
                //actualiza la lista a reporgramar
                for (TablaEventosPojo tep : eventosReprogramarList) {
                    CalendarioVacacion cv = new CalendarioVacacion();
                    cv = calendarioVacacionEJB.find(Integer.parseInt(tep.getEvento().getId()));
                    //cv.setId(1);
                    cv.setEstado(2);//reprogramado;
                    calendarioVacacionEJB.edit(cv);
                }
                System.out.println("Hecho !!!");
            } catch (Exception e) {
                System.out.println("Error al guardar ");
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Usuario debe de reprogramar todos los dias disponibles (" + totalDiasAcumuladosReprogramacion + ") de vacacion por reprogramacion !!!."));
        }
        return "calendario.xhtml?faces-redirect=true&includeViewParams=true";
    }
}

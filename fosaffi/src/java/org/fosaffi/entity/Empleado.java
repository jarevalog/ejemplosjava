/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jonathan
 */
@Entity
@Table(name = "PLA_EMPLEADO2")
@NamedQueries({
    @NamedQuery(name = "Empleado.findAll", query = "SELECT e FROM Empleado e"),
    @NamedQuery(name = "Empleado.findByCodEmp", query = "SELECT e FROM Empleado e WHERE e.empleadoPK.codEmp = :codEmp"),
    @NamedQuery(name = "Empleado.findByCodEmple", query = "SELECT e FROM Empleado e WHERE e.empleadoPK.codEmple = :codEmple"),
    @NamedQuery(name = "Empleado.findByCodUnidad", query = "SELECT e FROM Empleado e WHERE e.codUnidad = :codUnidad"),
    @NamedQuery(name = "Empleado.findByPrimNombre", query = "SELECT e FROM Empleado e WHERE e.primNombre = :primNombre"),
    @NamedQuery(name = "Empleado.findBySegNombre", query = "SELECT e FROM Empleado e WHERE e.segNombre = :segNombre"),
    @NamedQuery(name = "Empleado.findByPrimApelli", query = "SELECT e FROM Empleado e WHERE e.primApelli = :primApelli"),
    @NamedQuery(name = "Empleado.findBySegApelli", query = "SELECT e FROM Empleado e WHERE e.segApelli = :segApelli"),
    @NamedQuery(name = "Empleado.findByApeCasada", query = "SELECT e FROM Empleado e WHERE e.apeCasada = :apeCasada"),
    @NamedQuery(name = "Empleado.findByGenero", query = "SELECT e FROM Empleado e WHERE e.genero = :genero"),
    @NamedQuery(name = "Empleado.findByFechaNacim", query = "SELECT e FROM Empleado e WHERE e.fechaNacim = :fechaNacim"),
    @NamedQuery(name = "Empleado.findByEstadoCivil", query = "SELECT e FROM Empleado e WHERE e.estadoCivil = :estadoCivil"),
    @NamedQuery(name = "Empleado.findByFechaContrato", query = "SELECT e FROM Empleado e WHERE e.fechaContrato = :fechaContrato"),
    @NamedQuery(name = "Empleado.findByFechaRetiro", query = "SELECT e FROM Empleado e WHERE e.fechaRetiro = :fechaRetiro"),
    @NamedQuery(name = "Empleado.findByEstadoEmpl", query = "SELECT e FROM Empleado e WHERE e.estadoEmpl = :estadoEmpl"),
    @NamedQuery(name = "Empleado.findByPensionado", query = "SELECT e FROM Empleado e WHERE e.pensionado = :pensionado"),
    @NamedQuery(name = "Empleado.findByFechaCrea", query = "SELECT e FROM Empleado e WHERE e.fechaCrea = :fechaCrea"),
    @NamedQuery(name = "Empleado.findByUsuarioCrea", query = "SELECT e FROM Empleado e WHERE e.usuarioCrea = :usuarioCrea"),
    @NamedQuery(name = "Empleado.findByFechaMod", query = "SELECT e FROM Empleado e WHERE e.fechaMod = :fechaMod"),
    @NamedQuery(name = "Empleado.findByUsuarioMod", query = "SELECT e FROM Empleado e WHERE e.usuarioMod = :usuarioMod"),
    @NamedQuery(name = "Empleado.findByTipoContrato", query = "SELECT e FROM Empleado e WHERE e.tipoContrato = :tipoContrato")})
public class Empleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EmpleadoPK empleadoPK;
    @Column(name = "cod_unidad")
    private Integer codUnidad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "prim_nombre")
    private String primNombre;
    @Size(max = 20)
    @Column(name = "seg_nombre")
    private String segNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "prim_apelli")
    private String primApelli;
    @Size(max = 20)
    @Column(name = "seg_apelli")
    private String segApelli;
    @Size(max = 20)
    @Column(name = "ape_casada")
    private String apeCasada;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genero")
    private Character genero;
    @Column(name = "fecha_nacim")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacim;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado_civil")
    private Character estadoCivil;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_contrato")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaContrato;
    @Column(name = "fecha_retiro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRetiro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado_empl")
    private short estadoEmpl;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pensionado")
    private boolean pensionado;
    @Column(name = "fecha_crea")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCrea;
    @Size(max = 25)
    @Column(name = "usuario_crea")
    private String usuarioCrea;
    @Column(name = "fecha_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaMod;
    @Size(max = 25)
    @Column(name = "usuario_mod")
    private String usuarioMod;
    @Size(max = 25)
    @Column(name = "tipo_contrato")
    private String tipoContrato;

    public Empleado() {
    }

    public Empleado(EmpleadoPK empleadoPK) {
        this.empleadoPK = empleadoPK;
    }

    public Empleado(EmpleadoPK empleadoPK, String primNombre, String primApelli, Character genero, Character estadoCivil, Date fechaContrato, short estadoEmpl, boolean pensionado) {
        this.empleadoPK = empleadoPK;
        this.primNombre = primNombre;
        this.primApelli = primApelli;
        this.genero = genero;
        this.estadoCivil = estadoCivil;
        this.fechaContrato = fechaContrato;
        this.estadoEmpl = estadoEmpl;
        this.pensionado = pensionado;
    }

    public Empleado(int codEmp, int codEmple) {
        this.empleadoPK = new EmpleadoPK(codEmp, codEmple);
    }

    public EmpleadoPK getEmpleadoPK() {
        return empleadoPK;
    }

    public void setEmpleadoPK(EmpleadoPK empleadoPK) {
        this.empleadoPK = empleadoPK;
    }

    public Integer getCodUnidad() {
        return codUnidad;
    }

    public void setCodUnidad(Integer codUnidad) {
        this.codUnidad = codUnidad;
    }

    public String getPrimNombre() {
        return primNombre;
    }

    public void setPrimNombre(String primNombre) {
        this.primNombre = primNombre;
    }

    public String getSegNombre() {
        return segNombre;
    }

    public void setSegNombre(String segNombre) {
        this.segNombre = segNombre;
    }

    public String getPrimApelli() {
        return primApelli;
    }

    public void setPrimApelli(String primApelli) {
        this.primApelli = primApelli;
    }

    public String getSegApelli() {
        return segApelli;
    }

    public void setSegApelli(String segApelli) {
        this.segApelli = segApelli;
    }

    public String getApeCasada() {
        return apeCasada;
    }

    public void setApeCasada(String apeCasada) {
        this.apeCasada = apeCasada;
    }

    public Character getGenero() {
        return genero;
    }

    public void setGenero(Character genero) {
        this.genero = genero;
    }

    public Date getFechaNacim() {
        return fechaNacim;
    }

    public void setFechaNacim(Date fechaNacim) {
        this.fechaNacim = fechaNacim;
    }

    public Character getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(Character estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public Date getFechaContrato() {
        return fechaContrato;
    }

    public void setFechaContrato(Date fechaContrato) {
        this.fechaContrato = fechaContrato;
    }

    public Date getFechaRetiro() {
        return fechaRetiro;
    }

    public void setFechaRetiro(Date fechaRetiro) {
        this.fechaRetiro = fechaRetiro;
    }

    public short getEstadoEmpl() {
        return estadoEmpl;
    }

    public void setEstadoEmpl(short estadoEmpl) {
        this.estadoEmpl = estadoEmpl;
    }

    public boolean getPensionado() {
        return pensionado;
    }

    public void setPensionado(boolean pensionado) {
        this.pensionado = pensionado;
    }

    public Date getFechaCrea() {
        return fechaCrea;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Date fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuarioMod() {
        return usuarioMod;
    }

    public void setUsuarioMod(String usuarioMod) {
        this.usuarioMod = usuarioMod;
    }

    public String getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (empleadoPK != null ? empleadoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empleado)) {
            return false;
        }
        Empleado other = (Empleado) object;
        if ((this.empleadoPK == null && other.empleadoPK != null) || (this.empleadoPK != null && !this.empleadoPK.equals(other.empleadoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fosaffi.entity.Empleado[ empleadoPK=" + empleadoPK + " ]";
    }
    
}

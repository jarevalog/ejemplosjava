/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jonathan
 */
@Entity
@Table(name = "empleado_unidad")
@NamedQueries({
    @NamedQuery(name = "Unidad.findAll", query = "SELECT u FROM Unidad u"),
    @NamedQuery(name = "Unidad.findByCodEmp", query = "SELECT u FROM Unidad u WHERE u.unidadPK.codEmp = :codEmp"),
    @NamedQuery(name = "Unidad.findByCodUnidad", query = "SELECT u FROM Unidad u WHERE u.unidadPK.codUnidad = :codUnidad"),
    @NamedQuery(name = "Unidad.findByCodEmple", query = "SELECT u FROM Unidad u WHERE u.unidadPK.codEmple = :codEmple"),
    @NamedQuery(name = "Unidad.findByIndJefe", query = "SELECT u FROM Unidad u WHERE u.indJefe = :indJefe"),
    @NamedQuery(name = "Unidad.findByFechacontrol", query = "SELECT u FROM Unidad u WHERE u.fechacontrol = :fechacontrol")})
public class Unidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UnidadPK unidadPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ind_jefe")
    private boolean indJefe;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechacontrol")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechacontrol;

    public Unidad() {
    }

    public Unidad(UnidadPK unidadPK) {
        this.unidadPK = unidadPK;
    }

    public Unidad(UnidadPK unidadPK, boolean indJefe, Date fechacontrol) {
        this.unidadPK = unidadPK;
        this.indJefe = indJefe;
        this.fechacontrol = fechacontrol;
    }

    public Unidad(int codEmp, int codUnidad, short codEmple) {
        this.unidadPK = new UnidadPK(codEmp, codUnidad, codEmple);
    }

    public UnidadPK getUnidadPK() {
        return unidadPK;
    }

    public void setUnidadPK(UnidadPK unidadPK) {
        this.unidadPK = unidadPK;
    }

    public boolean getIndJefe() {
        return indJefe;
    }

    public void setIndJefe(boolean indJefe) {
        this.indJefe = indJefe;
    }

    public Date getFechacontrol() {
        return fechacontrol;
    }

    public void setFechacontrol(Date fechacontrol) {
        this.fechacontrol = fechacontrol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (unidadPK != null ? unidadPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Unidad)) {
            return false;
        }
        Unidad other = (Unidad) object;
        if ((this.unidadPK == null && other.unidadPK != null) || (this.unidadPK != null && !this.unidadPK.equals(other.unidadPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fosaffi.entity.Unidad[ unidadPK=" + unidadPK + " ]";
    }
    
}

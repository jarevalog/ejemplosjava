/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author jonathan
 */
@Entity
@Table(name = "pla_vacacion_empleado")
@NamedQueries({
    @NamedQuery(name = "VacacionEmpleado.findAll", query = "SELECT v FROM VacacionEmpleado v"),
    @NamedQuery(name = "VacacionEmpleado.findByCodEmp", query = "SELECT v FROM VacacionEmpleado v WHERE v.vacacionEmpleadoPK.codEmp = :codEmp"),
    @NamedQuery(name = "VacacionEmpleado.findByCodEmple", query = "SELECT v FROM VacacionEmpleado v WHERE v.vacacionEmpleadoPK.codEmple = :codEmple"),
    @NamedQuery(name = "VacacionEmpleado.findByCodPeriodo", query = "SELECT v FROM VacacionEmpleado v WHERE v.vacacionEmpleadoPK.codPeriodo = :codPeriodo"),
    @NamedQuery(name = "VacacionEmpleado.findByDiasVacacion", query = "SELECT v FROM VacacionEmpleado v WHERE v.diasVacacion = :diasVacacion"),
    @NamedQuery(name = "VacacionEmpleado.findByDiasCorresponden", query = "SELECT v FROM VacacionEmpleado v WHERE v.diasCorresponden = :diasCorresponden"),
    @NamedQuery(name = "VacacionEmpleado.findByFechaControl", query = "SELECT v FROM VacacionEmpleado v WHERE v.fechaControl = :fechaControl")})
public class VacacionEmpleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VacacionEmpleadoPK vacacionEmpleadoPK;
    @Column(name = "dias_vacacion")
    private Integer diasVacacion;
    @Column(name = "dias_corresponden")
    private Integer diasCorresponden;
    @Column(name = "fecha_control")
    @Temporal(TemporalType.DATE)
    private Date fechaControl;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vacacionEmpleado", fetch = FetchType.LAZY)
    private List<Programa> programaList;
    @JoinColumn(name = "cod_periodo", referencedColumnName = "cod_periodo", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private PeriodoVacacion periodoVacacion;

    public VacacionEmpleado() {
    }

    public VacacionEmpleado(VacacionEmpleadoPK vacacionEmpleadoPK) {
        this.vacacionEmpleadoPK = vacacionEmpleadoPK;
    }

    public VacacionEmpleado(int codEmp, int codEmple, int codPeriodo) {
        this.vacacionEmpleadoPK = new VacacionEmpleadoPK(codEmp, codEmple, codPeriodo);
    }

    public VacacionEmpleadoPK getVacacionEmpleadoPK() {
        return vacacionEmpleadoPK;
    }

    public void setVacacionEmpleadoPK(VacacionEmpleadoPK vacacionEmpleadoPK) {
        this.vacacionEmpleadoPK = vacacionEmpleadoPK;
    }

    public Integer getDiasVacacion() {
        return diasVacacion;
    }

    public void setDiasVacacion(Integer diasVacacion) {
        this.diasVacacion = diasVacacion;
    }

    public Integer getDiasCorresponden() {
        return diasCorresponden;
    }

    public void setDiasCorresponden(Integer diasCorresponden) {
        this.diasCorresponden = diasCorresponden;
    }

    public Date getFechaControl() {
        return fechaControl;
    }

    public void setFechaControl(Date fechaControl) {
        this.fechaControl = fechaControl;
    }

    public List<Programa> getProgramaList() {
        return programaList;
    }

    public void setProgramaList(List<Programa> programaList) {
        this.programaList = programaList;
    }

    public PeriodoVacacion getPeriodoVacacion() {
        return periodoVacacion;
    }

    public void setPeriodoVacacion(PeriodoVacacion periodoVacacion) {
        this.periodoVacacion = periodoVacacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vacacionEmpleadoPK != null ? vacacionEmpleadoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VacacionEmpleado)) {
            return false;
        }
        VacacionEmpleado other = (VacacionEmpleado) object;
        if ((this.vacacionEmpleadoPK == null && other.vacacionEmpleadoPK != null) || (this.vacacionEmpleadoPK != null && !this.vacacionEmpleadoPK.equals(other.vacacionEmpleadoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fosaffi.entity.VacacionEmpleado[ vacacionEmpleadoPK=" + vacacionEmpleadoPK + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author jonathan
 */
@Entity
@Table(name = "pla_vacacion_programa")
@NamedQueries({
    @NamedQuery(name = "Programa.findAll", query = "SELECT p FROM Programa p"),
    @NamedQuery(name = "Programa.findByKey", query = "SELECT p FROM Programa p WHERE p.programaPK in :pk")
})
public class Programa implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProgramaPK programaPK;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "dias_pendientes")
    private Integer diasPendientes;
    @Column(name = "fecha_control")
    @Temporal(TemporalType.DATE)
    private Date fechaControl;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "programa", fetch = FetchType.LAZY)
    private List<CalendarioVacacion> calendarioVacacionList;
    @JoinColumn(name = "tipo", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoPrograma tipoPrograma;
    @JoinColumns({
        @JoinColumn(name = "cod_emp", referencedColumnName = "cod_emp", insertable = false, updatable = false),
        @JoinColumn(name = "cod_emple", referencedColumnName = "cod_emple", insertable = false, updatable = false),
        @JoinColumn(name = "cod_periodo", referencedColumnName = "cod_periodo", insertable = false, updatable = false)})
    @ManyToOne(optional = false, fetch = FetchType.LAZY,cascade = {CascadeType.MERGE})
    private VacacionEmpleado vacacionEmpleado;

    public Programa() {
    }

    public Programa(ProgramaPK programaPK) {
        this.programaPK = programaPK;
    }

    public Programa(int codEmp, int codEmple, int codPeriodo, int tipo) {
        this.programaPK = new ProgramaPK(codEmp, codEmple, codPeriodo, tipo);
    }

    public ProgramaPK getProgramaPK() {
        return programaPK;
    }

    public void setProgramaPK(ProgramaPK programaPK) {
        this.programaPK = programaPK;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getDiasPendientes() {
        return diasPendientes;
    }

    public void setDiasPendientes(Integer diasPendientes) {
        this.diasPendientes = diasPendientes;
    }

    public Date getFechaControl() {
        return fechaControl;
    }

    public void setFechaControl(Date fechaControl) {
        this.fechaControl = fechaControl;
    }

    public List<CalendarioVacacion> getCalendarioVacacionList() {
        return calendarioVacacionList;
    }

    public void setCalendarioVacacionList(List<CalendarioVacacion> calendarioVacacionList) {
        this.calendarioVacacionList = calendarioVacacionList;
    }

    public TipoPrograma getTipoPrograma() {
        return tipoPrograma;
    }

    public void setTipoPrograma(TipoPrograma tipoPrograma) {
        this.tipoPrograma = tipoPrograma;
    }

    public VacacionEmpleado getVacacionEmpleado() {
        return vacacionEmpleado;
    }

    public void setVacacionEmpleado(VacacionEmpleado vacacionEmpleado) {
        this.vacacionEmpleado = vacacionEmpleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (programaPK != null ? programaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Programa)) {
            return false;
        }
        Programa other = (Programa) object;
        if ((this.programaPK == null && other.programaPK != null) || (this.programaPK != null && !this.programaPK.equals(other.programaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fosaffi.entity.Programa[ programaPK=" + programaPK + " ]";
    }
    
}

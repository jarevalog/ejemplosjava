/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jonathan
 */
@Entity
@Table(name = "pla_estado_vacacion_calendario")
@NamedQueries({
    @NamedQuery(name = "EstadoVacacion.findAll", query = "SELECT e FROM EstadoVacacion e"),
    @NamedQuery(name = "EstadoVacacion.findById", query = "SELECT e FROM EstadoVacacion e WHERE e.id = :id"),
    @NamedQuery(name = "EstadoVacacion.findByEstado", query = "SELECT e FROM EstadoVacacion e WHERE e.estado = :estado")})
public class EstadoVacacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "estado")
    private String estado;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    /*@OneToMany(cascade = CascadeType.ALL, mappedBy = "tipo", fetch = FetchType.LAZY)
    private List<CalendarioVacacion> calendarioVacacionList;*/

    public EstadoVacacion() {
    }

    public EstadoVacacion(Integer id) {
        this.id = id;
    }

    public EstadoVacacion(Integer id, String estado) {
        this.id = id;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /*public List<CalendarioVacacion> getCalendarioVacacionList() {
        return calendarioVacacionList;
    }

    public void setCalendarioVacacionList(List<CalendarioVacacion> calendarioVacacionList) {
        this.calendarioVacacionList = calendarioVacacionList;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadoVacacion)) {
            return false;
        }
        EstadoVacacion other = (EstadoVacacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fosaffi.entity.EstadoVacacion[ id=" + id + " ]";
    }
    
}

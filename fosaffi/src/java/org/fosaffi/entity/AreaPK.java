/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jonathan
 */
@Embeddable
public class AreaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "cod_emp")
    private int codEmp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cod_unidad")
    private int codUnidad;

    public AreaPK() {
    }

    public AreaPK(int codEmp, int codUnidad) {
        this.codEmp = codEmp;
        this.codUnidad = codUnidad;
    }

    public int getCodEmp() {
        return codEmp;
    }

    public void setCodEmp(int codEmp) {
        this.codEmp = codEmp;
    }

    public int getCodUnidad() {
        return codUnidad;
    }

    public void setCodUnidad(int codUnidad) {
        this.codUnidad = codUnidad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) codEmp;
        hash += (int) codUnidad;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AreaPK)) {
            return false;
        }
        AreaPK other = (AreaPK) object;
        if (this.codEmp != other.codEmp) {
            return false;
        }
        if (this.codUnidad != other.codUnidad) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fosaffi.entity.AreaPK[ codEmp=" + codEmp + ", codUnidad=" + codUnidad + " ]";
    }
    
}

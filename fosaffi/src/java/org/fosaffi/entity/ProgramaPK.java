/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jonathan
 */
@Embeddable
public class ProgramaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "cod_emp")
    private int codEmp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cod_emple")
    private int codEmple;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cod_periodo")
    private int codPeriodo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipo")
    private int tipo;

    public ProgramaPK() {
    }

    public ProgramaPK(int codEmp, int codEmple, int codPeriodo, int tipo) {
        this.codEmp = codEmp;
        this.codEmple = codEmple;
        this.codPeriodo = codPeriodo;
        this.tipo = tipo;
    }

    public int getCodEmp() {
        return codEmp;
    }

    public void setCodEmp(int codEmp) {
        this.codEmp = codEmp;
    }

    public int getCodEmple() {
        return codEmple;
    }

    public void setCodEmple(int codEmple) {
        this.codEmple = codEmple;
    }

    public int getCodPeriodo() {
        return codPeriodo;
    }

    public void setCodPeriodo(int codPeriodo) {
        this.codPeriodo = codPeriodo;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) codEmp;
        hash += (int) codEmple;
        hash += (int) codPeriodo;
        hash += (int) tipo;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProgramaPK)) {
            return false;
        }
        ProgramaPK other = (ProgramaPK) object;
        if (this.codEmp != other.codEmp) {
            return false;
        }
        if (this.codEmple != other.codEmple) {
            return false;
        }
        if (this.codPeriodo != other.codPeriodo) {
            return false;
        }
        if (this.tipo != other.tipo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fosaffi.entity.ProgramaPK[ codEmp=" + codEmp + ", codEmple=" + codEmple + ", codPeriodo=" + codPeriodo + ", tipo=" + tipo + " ]";
    }
    
}

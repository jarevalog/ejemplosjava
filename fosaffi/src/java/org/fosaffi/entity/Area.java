/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jonathan
 */
@Entity
@Table(name = "AREA_INSTITUC2")
@NamedQueries({
    @NamedQuery(name = "Area.findAll", query = "SELECT a FROM Area a"),
    @NamedQuery(name = "Area.findByCodEmp", query = "SELECT a FROM Area a WHERE a.areaPK.codEmp = :codEmp"),
    @NamedQuery(name = "Area.findByCodUnidad", query = "SELECT a FROM Area a WHERE a.areaPK.codUnidad = :codUnidad"),
    @NamedQuery(name = "Area.findByDescripcion", query = "SELECT a FROM Area a WHERE a.descripcion = :descripcion"),
    @NamedQuery(name = "Area.findByDptoPadre", query = "SELECT a FROM Area a WHERE a.dptoPadre = :dptoPadre"),
    @NamedQuery(name = "Area.findByUnidadActiva", query = "SELECT a FROM Area a WHERE a.unidadActiva = :unidadActiva"),
    @NamedQuery(name = "Area.findByTelefonoUnidad", query = "SELECT a FROM Area a WHERE a.telefonoUnidad = :telefonoUnidad"),
    @NamedQuery(name = "Area.findByFechaCrea", query = "SELECT a FROM Area a WHERE a.fechaCrea = :fechaCrea"),
    @NamedQuery(name = "Area.findByUsuarioCrea", query = "SELECT a FROM Area a WHERE a.usuarioCrea = :usuarioCrea"),
    @NamedQuery(name = "Area.findByFechaMod", query = "SELECT a FROM Area a WHERE a.fechaMod = :fechaMod"),
    @NamedQuery(name = "Area.findByUsuarioMod", query = "SELECT a FROM Area a WHERE a.usuarioMod = :usuarioMod")})
public class Area implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AreaPK areaPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "dpto_padre")
    private Short dptoPadre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "unidad_activa")
    private boolean unidadActiva;
    @Size(max = 10)
    @Column(name = "telefono_unidad")
    private String telefonoUnidad;
    @Column(name = "fecha_crea")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCrea;
    @Size(max = 25)
    @Column(name = "usuario_crea")
    private String usuarioCrea;
    @Column(name = "fecha_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaMod;
    @Size(max = 25)
    @Column(name = "usuario_mod")
    private String usuarioMod;

    public Area() {
    }

    public Area(AreaPK areaPK) {
        this.areaPK = areaPK;
    }

    public Area(AreaPK areaPK, String descripcion, boolean unidadActiva) {
        this.areaPK = areaPK;
        this.descripcion = descripcion;
        this.unidadActiva = unidadActiva;
    }

    /*public Area(int codEmp, int codEmple, int codUnidad) {
        this.areaPK = new AreaPK(codEmp, codEmple, codUnidad);
    }*/

    public AreaPK getAreaPK() {
        return areaPK;
    }

    public void setAreaPK(AreaPK areaPK) {
        this.areaPK = areaPK;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Short getDptoPadre() {
        return dptoPadre;
    }

    public void setDptoPadre(Short dptoPadre) {
        this.dptoPadre = dptoPadre;
    }

    public boolean getUnidadActiva() {
        return unidadActiva;
    }

    public void setUnidadActiva(boolean unidadActiva) {
        this.unidadActiva = unidadActiva;
    }

    public String getTelefonoUnidad() {
        return telefonoUnidad;
    }

    public void setTelefonoUnidad(String telefonoUnidad) {
        this.telefonoUnidad = telefonoUnidad;
    }

    public Date getFechaCrea() {
        return fechaCrea;
    }

    public void setFechaCrea(Date fechaCrea) {
        this.fechaCrea = fechaCrea;
    }

    public String getUsuarioCrea() {
        return usuarioCrea;
    }

    public void setUsuarioCrea(String usuarioCrea) {
        this.usuarioCrea = usuarioCrea;
    }

    public Date getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Date fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuarioMod() {
        return usuarioMod;
    }

    public void setUsuarioMod(String usuarioMod) {
        this.usuarioMod = usuarioMod;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (areaPK != null ? areaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Area)) {
            return false;
        }
        Area other = (Area) object;
        if ((this.areaPK == null && other.areaPK != null) || (this.areaPK != null && !this.areaPK.equals(other.areaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fosaffi.entity.Area[ areaPK=" + areaPK + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jonathan
 */
@Entity
@Table(name = "pla_vacacion_periodo")
@NamedQueries({
    @NamedQuery(name = "PeriodoVacacion.findAll", query = "SELECT p FROM PeriodoVacacion p"),
    @NamedQuery(name = "PeriodoVacacion.findByCodPeriodo", query = "SELECT p FROM PeriodoVacacion p WHERE p.codPeriodo = :codPeriodo"),
    @NamedQuery(name = "PeriodoVacacion.findByDiasColectivos", query = "SELECT p FROM PeriodoVacacion p WHERE p.diasColectivos = :diasColectivos"),
    @NamedQuery(name = "PeriodoVacacion.findByFechaControl", query = "SELECT p FROM PeriodoVacacion p WHERE p.fechaControl = :fechaControl")})
public class PeriodoVacacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cod_periodo")
    private Integer codPeriodo;
    @Column(name = "dias_colectivos")
    private Integer diasColectivos;
    @Column(name = "fecha_control")
    @Temporal(TemporalType.DATE)
    private Date fechaControl;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "periodoVacacion", fetch = FetchType.LAZY)
    private List<VacacionEmpleado> vacacionEmpleadoList;

    public PeriodoVacacion() {
    }

    public PeriodoVacacion(Integer codPeriodo) {
        this.codPeriodo = codPeriodo;
    }

    public Integer getCodPeriodo() {
        return codPeriodo;
    }

    public void setCodPeriodo(Integer codPeriodo) {
        this.codPeriodo = codPeriodo;
    }

    public Integer getDiasColectivos() {
        return diasColectivos;
    }

    public void setDiasColectivos(Integer diasColectivos) {
        this.diasColectivos = diasColectivos;
    }

    public Date getFechaControl() {
        return fechaControl;
    }

    public void setFechaControl(Date fechaControl) {
        this.fechaControl = fechaControl;
    }

    public List<VacacionEmpleado> getVacacionEmpleadoList() {
        return vacacionEmpleadoList;
    }

    public void setVacacionEmpleadoList(List<VacacionEmpleado> vacacionEmpleadoList) {
        this.vacacionEmpleadoList = vacacionEmpleadoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codPeriodo != null ? codPeriodo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PeriodoVacacion)) {
            return false;
        }
        PeriodoVacacion other = (PeriodoVacacion) object;
        if ((this.codPeriodo == null && other.codPeriodo != null) || (this.codPeriodo != null && !this.codPeriodo.equals(other.codPeriodo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fosaffi.entity.PeriodoVacacion[ codPeriodo=" + codPeriodo + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jonathan
 */
@Embeddable
public class UnidadPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "cod_emp")
    private int codEmp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cod_unidad")
    private int codUnidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cod_emple")
    private short codEmple;

    public UnidadPK() {
    }

    public UnidadPK(int codEmp, int codUnidad, short codEmple) {
        this.codEmp = codEmp;
        this.codUnidad = codUnidad;
        this.codEmple = codEmple;
    }

    public int getCodEmp() {
        return codEmp;
    }

    public void setCodEmp(int codEmp) {
        this.codEmp = codEmp;
    }

    public int getCodUnidad() {
        return codUnidad;
    }

    public void setCodUnidad(int codUnidad) {
        this.codUnidad = codUnidad;
    }

    public short getCodEmple() {
        return codEmple;
    }

    public void setCodEmple(short codEmple) {
        this.codEmple = codEmple;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) codEmp;
        hash += (int) codUnidad;
        hash += (int) codEmple;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UnidadPK)) {
            return false;
        }
        UnidadPK other = (UnidadPK) object;
        if (this.codEmp != other.codEmp) {
            return false;
        }
        if (this.codUnidad != other.codUnidad) {
            return false;
        }
        if (this.codEmple != other.codEmple) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fosaffi.entity.UnidadPK[ codEmp=" + codEmp + ", codUnidad=" + codUnidad + ", codEmple=" + codEmple + " ]";
    }
    
}

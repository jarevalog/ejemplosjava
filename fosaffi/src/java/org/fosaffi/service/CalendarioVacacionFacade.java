/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.fosaffi.entity.Area;
import org.fosaffi.entity.CalendarioVacacion;
import org.fosaffi.entity.Programa;
import org.fosaffi.entity.ProgramaPK;
import org.fosaffi.pojo.TablaEventosPojo;
import org.fosaffi.pojo.Usuario;
import org.primefaces.model.DefaultScheduleEvent;

/**
 *
 * @author jonathan
 */
@Stateless
public class CalendarioVacacionFacade extends AbstractFacade<CalendarioVacacion> implements CalendarioVacacionFacadeLocal {

    @EJB
    AreaFacadeLocal areaEJB;

    @PersistenceContext(unitName = "fosaffiPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CalendarioVacacionFacade() {
        super(CalendarioVacacion.class);
    }

    public List<CalendarioVacacion> findVacacionesByEmpleadoProgram(List<Programa> programas) {
        List<CalendarioVacacion> results = new ArrayList<>();
        try {
            results = em.createNamedQuery("CalendarioVacacion.findByProgramas", CalendarioVacacion.class)
                    .setParameter("programas", programas).getResultList();
        } catch (Exception e) {
            // log
            System.err.println("Error " + e.getMessage());
        }
        return results;
    }

    public List<TablaEventosPojo> findVacacionesByUnidad(Usuario usuario) {
        List<Integer> unidad = new ArrayList<>();
        if (usuario.getRol().equals("EMPLEADO") || usuario.getRol().equals("JEFE")) {
            unidad.add(usuario.getCodigoUnidad());
        }
        if (usuario.getRol().equals("RRHH")) {
            List<Area> lstArea = areaEJB.findAll();
            lstArea.forEach(item -> unidad.add(item.getAreaPK().getCodUnidad()));
        }

        List<TablaEventosPojo> lst = new ArrayList<>();
        String query = "select [cod_emple]  "
                + "      ,[fecha_desde]  "
                + "      ,[fecha_hasta]  "
                + "      ,[estado],(select prim_nombre+' '+seg_nombre+' '+prim_apelli from PLA_EMPLEADO2  pe where pe.cod_emple=pvc.cod_emple and pe.cod_unidad in :u)"
                + ",id from pla_vacacion_calendario pvc "
                + "where cod_emple in( "
                + "select cod_emple from PLA_EMPLEADO2 "
                + "where cod_unidad in :u)"
                + " order by 2 asc ";

        Query q = em.createNativeQuery(query);
        q.setParameter("u", unidad);

        List<Object[]> results = q.getResultList();
        for (Object[] result : results) {
            Date desde = (Date) result[1];
            Date hasta = (Date) result[2];
            DefaultScheduleEvent evento = new DefaultScheduleEvent(result[4].toString(), desde, hasta);
            evento.setId(result[5].toString());
            TablaEventosPojo tep = new TablaEventosPojo();
            tep.setEvento(evento);
            lst.add(tep);
        }
        return lst;
    }


}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fosaffi.entity.VacacionEmpleado;

/**
 *
 * @author jonathan
 */
@Stateless
public class VacacionEmpleadoFacade extends AbstractFacade<VacacionEmpleado> implements VacacionEmpleadoFacadeLocal {

    @PersistenceContext(unitName = "fosaffiPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VacacionEmpleadoFacade() {
        super(VacacionEmpleado.class);
    }

    public VacacionEmpleado findByPeriodo(Integer year) {
        VacacionEmpleado result = new VacacionEmpleado();
        try {
            result = em.createNamedQuery("VacacionEmpleado.findByCodPeriodo", VacacionEmpleado.class)
                    .setParameter("year", year).getSingleResult();
        } catch (Exception e) {
            // log
            System.err.println("Error " + e.getMessage());
        }
        return result;
    }

}

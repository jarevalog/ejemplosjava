/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.service;

import java.util.List;
import javax.ejb.Local;
import org.fosaffi.entity.VacacionEmpleado;

/**
 *
 * @author jonathan
 */
@Local
public interface VacacionEmpleadoFacadeLocal {

    void create(VacacionEmpleado vacacionEmpleado);

    void edit(VacacionEmpleado vacacionEmpleado);

    void remove(VacacionEmpleado vacacionEmpleado);

    VacacionEmpleado find(Object id);

    List<VacacionEmpleado> findAll();

    List<VacacionEmpleado> findRange(int[] range);

    int count();

    public VacacionEmpleado findByPeriodo(Integer year);
    
}

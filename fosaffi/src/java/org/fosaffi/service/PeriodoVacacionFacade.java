/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fosaffi.entity.PeriodoVacacion;

/**
 *
 * @author jonathan
 */
@Stateless
public class PeriodoVacacionFacade extends AbstractFacade<PeriodoVacacion> implements PeriodoVacacionFacadeLocal {

    @PersistenceContext(unitName = "fosaffiPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PeriodoVacacionFacade() {
        super(PeriodoVacacion.class);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.service;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fosaffi.entity.Programa;
import org.fosaffi.entity.ProgramaPK;

/**
 *
 * @author jonathan
 */
@Stateless
public class ProgramaFacade extends AbstractFacade<Programa> implements ProgramaFacadeLocal {

    @PersistenceContext(unitName = "fosaffiPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProgramaFacade() {
        super(Programa.class);
    }

    public List<Programa> findProgramasByEmpleadoPeriodo(Integer codEmp, Integer codEmple, Integer periodo) {
        List<ProgramaPK> lstPk = new ArrayList<>();
        List<Programa> results = new ArrayList<>();
        try {
            lstPk.add(new ProgramaPK(codEmp, codEmple, periodo, 0));
            lstPk.add(new ProgramaPK(codEmp, codEmple, periodo, 1));
            lstPk.add(new ProgramaPK(codEmp, codEmple, periodo, 2));
            lstPk.add(new ProgramaPK(codEmp, codEmple, periodo, 3));
            results = em.createNamedQuery("Programa.findByKey", Programa.class)
                    .setParameter("pk", lstPk).getResultList();
        } catch (Exception e) {
            // log
            System.err.println("Error " + e.getMessage());
        }
        return results;
    }

}

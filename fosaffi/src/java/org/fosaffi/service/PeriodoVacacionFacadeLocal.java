/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.service;

import java.util.List;
import javax.ejb.Local;
import org.fosaffi.entity.PeriodoVacacion;

/**
 *
 * @author jonathan
 */
@Local
public interface PeriodoVacacionFacadeLocal {

    void create(PeriodoVacacion periodoVacacion);

    void edit(PeriodoVacacion periodoVacacion);

    void remove(PeriodoVacacion periodoVacacion);

    PeriodoVacacion find(Object id);

    List<PeriodoVacacion> findAll();

    List<PeriodoVacacion> findRange(int[] range);

    int count();
    
}

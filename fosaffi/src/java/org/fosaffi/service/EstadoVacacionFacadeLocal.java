/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.service;

import java.util.List;
import javax.ejb.Local;
import org.fosaffi.entity.EstadoVacacion;

/**
 *
 * @author jonathan
 */
@Local
public interface EstadoVacacionFacadeLocal {

    void create(EstadoVacacion estadoVacacion);

    void edit(EstadoVacacion estadoVacacion);

    void remove(EstadoVacacion estadoVacacion);

    EstadoVacacion find(Object id);

    List<EstadoVacacion> findAll();

    List<EstadoVacacion> findRange(int[] range);

    int count();
    
}

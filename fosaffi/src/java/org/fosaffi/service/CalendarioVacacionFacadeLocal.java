/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.service;

import java.util.List;
import javax.ejb.Local;
import org.fosaffi.entity.CalendarioVacacion;
import org.fosaffi.entity.Programa;
import org.fosaffi.entity.ProgramaPK;
import org.fosaffi.pojo.TablaEventosPojo;
import org.fosaffi.pojo.Usuario;

/**
 *
 * @author jonathan
 */
@Local
public interface CalendarioVacacionFacadeLocal {

    void create(CalendarioVacacion calendarioVacacion);

    void edit(CalendarioVacacion calendarioVacacion);

    void remove(CalendarioVacacion calendarioVacacion);

    CalendarioVacacion find(Object id);

    List<CalendarioVacacion> findAll();

    List<CalendarioVacacion> findRange(int[] range);

    int count();

    public List<CalendarioVacacion> findVacacionesByEmpleadoProgram(List<Programa> programas);

    public List<TablaEventosPojo> findVacacionesByUnidad(Usuario usuario);

}

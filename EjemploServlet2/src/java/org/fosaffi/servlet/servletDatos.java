/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fosaffi.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.fosaffi.conexion.Conectar;

/**
 *
 * @author jonathan
 */
@WebServlet(name = "servletDatos", urlPatterns = {"/servletDatos"})
public class servletDatos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>"
                    + "<style>"
                    + "table {"
                    + "   width: 100%;"
                    + "   border: 1px solid #000;"
                    + "}"
                    + "th, td {"
                    + "   width: 25%;"
                    + "   text-align: left;"
                    + "   vertical-align: top;"
                    + "   border: 1px solid #000;"
                    + "   border-spacing: 0;"
                    + "}"
                    + "</style>");
            out.println("<title>Servlet servletDatos</title>");
            out.println("</head>");
            out.println("<body><br><br>");
            out.println("<table>");
            out.println("<tr>"
                    + "<td>ID</td>"
                    + "<td>NOMBRE</td>"
                    + "<td>STOCK</td>"
                    + "<td>CATEGORIA</td>"
                    + "<td>ESTADO</td>"
                    + "</tr>");
            //conectarse a la base de datos y consultar la tabla productos.
            Connection con = null;
            Conectar c = new Conectar();
            con = c.conectarBase();
            Statement s = null;
            ResultSet rs = null;
            try {
                s = con.createStatement();
                rs = s.executeQuery("select * from productos");
                while (rs.next()) {
                    out.println("<tr>");
                    out.println("<td>");
                    out.println(rs.getInt("id_producto"));
                    out.println("</td>");
                    out.println("<td>");
                    out.println(rs.getString("nombre"));
                    out.println("</td>");
                    out.println("<td>");
//                    System.out.print("\t");
                    out.println(rs.getFloat("stock"));
                    out.println("</td>");
                    out.println("<td>");
//                    System.out.print("\t\t");
                    out.println(rs.getInt("id_categoria"));
                    out.println("</td>");
                    out.println("<td>");
//                    System.out.print("\t");
                    out.println(rs.getInt("id_estado"));
                    out.println("</td>");
                    //System.out.println("");
                    out.println("</tr>");
                }
            } catch (Exception e) {
                System.err.println("Error. " + e.getMessage());
            } finally {
                try {
                    con.close();
                } catch (SQLException ex) {
                    System.err.println("Error" + ex.getMessage());
                }
                try {
                    s.close();
                } catch (SQLException ex) {
                    System.err.println("Error" + ex.getMessage());
                }
                try {
                    rs.close();
                } catch (SQLException ex) {
                    System.err.println("Error" + ex.getMessage());
                }
            }
            out.println("</table>");
            //out.println("<h1>Servlet servletDatos at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
